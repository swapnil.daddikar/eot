package com.game;

import java.util.Scanner;

public class GameScanner {
    private Scanner scanner;

    public GameScanner() {
    }

    public GameScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public String next() {
        return scanner.next();
    }
}
