package com.game;

public interface Game {

    public void start();
    public void end();

}
