package com.game;

import com.mapper.MoveScore;
import com.machine.Machine;
import com.player.ConsolePlayer;
import com.player.Observer;
import com.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EvolutionOfTrust implements Game {

    private Player playerOne;
    private Player playerTwo;
    private GameScanner scanner;
    private static List<Observer> observers=new ArrayList<Observer>();


    public EvolutionOfTrust(Player playerOne, Player playerTwo, GameScanner scanner) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.scanner = scanner;
    }

    public static void addObserver(Observer observer) {
        observers.add(observer);
    }

    private static void publishToObservers(String move1 , String move2){
        for(Observer observer : observers){
            observer.listenMoves(move1 , move2);
        }
    }

    @Override
    public void start() {
        int rounds = 1;
        boolean continuePlaying = true;
        while (continuePlaying) {
            System.out.println("Player 1 move -> ");
            String playerOneMove = playerOne.makeMove();
            System.out.println("Player 2 move -> ");
            String playerTwoMove = playerTwo.makeMove();
            MoveScore moveScore = Machine.calculate(playerOneMove, playerTwoMove);
            this.playerOne.updateScore(moveScore.getPlayerOneMoveScore());
            this.playerTwo.updateScore(moveScore.getPlayerTwoMoveScore());
            showScore(rounds);
            publishToObservers(playerOneMove,playerTwoMove);
            rounds = rounds + 1;
            continuePlaying = continuePlay();
        }


    }


    private boolean continuePlay() {
        System.out.println("Continue Play ? Y/N");
        return scanner.next().equals("Y");
    }


    /*
       Need to implement
     */
    @Override
    public void end() {

    }


    private void showScore(int rounds) {
        System.out.println(playerOne.getName() + " has a score of " + playerOne.getScore() + " in round " + rounds);
        System.out.println(playerTwo.getName() + " has a score of " + playerTwo.getScore() + " in round " + rounds);
    }


    public static void main(String[] args) {
        Game game = new EvolutionOfTrust(new ConsolePlayer("PlayerOne", 0, new GameScanner(new Scanner(System.in))),
                new ConsolePlayer("PlayerTwo", 0, new GameScanner(new Scanner(System.in)))
                , new GameScanner(new Scanner(System.in)));
        game.start();
    }


    /*
       Game class is the publisher.
       This will have an update method.
       Add copycat instance in list of observers.
       CopyCat will implement observer interface.
       Game will call update on all observers which are registered.
     */
}
