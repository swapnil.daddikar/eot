package com.player;

import com.game.GameScanner;

public class ConsolePlayer extends Player {

    private GameScanner scanner;

    public ConsolePlayer(String name, int score, GameScanner scanner) {
        super(name, score);
        this.scanner = scanner;
    }

    public String makeMove() {
        return scanner.next();
    }


}
