package com.player;

public class CheatPlayer extends Player {

    public CheatPlayer(String name, int score) {
        super(name,score);
    }

    public String makeMove() {
        return "ch";
    }

}
