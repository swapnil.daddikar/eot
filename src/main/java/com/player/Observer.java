package com.player;

public interface Observer {
    public void listenMoves(String move1, String move2);
}
