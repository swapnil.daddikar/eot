package com.player;

public class CooperatePlayer extends Player {

    public CooperatePlayer(String name, int score) {
        super(name, score);
    }

    public String makeMove() {
        return "co";
    }

}
