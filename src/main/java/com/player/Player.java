package com.player;

public abstract class Player{

    private final String name;
    private int score;

    public Player(String name ,int score) {
        this.name = name;
        this.score = score;
    }

    public void updateScore(int gameScore) {
        this.score += gameScore;
    }

    public int getScore() {
        return this.score;
    }

    public String getName() {
        return this.name;
    }

    public abstract String makeMove();

}
