package com.player;

import com.game.EvolutionOfTrust;

public class CopyCatPlayer extends Player implements Observer {

    private String move = "co";

    public CopyCatPlayer(String name, int score) {
        super(name, score);
        EvolutionOfTrust.addObserver(this);
    }


    @Override
    public String makeMove() {
        return move;
    }

    @Override
    public void listenMoves(String move1, String move2) {
        if (!move1.equals(move2)) {
            if (move.equals(move1)) {
                move = move2;
            } else {
                move = move1;
            }
        }
    }
}
