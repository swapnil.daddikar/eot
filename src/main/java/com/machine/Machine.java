package com.machine;

import com.mapper.MoveMapper;
import com.mapper.MoveScore;

public class Machine {

    private static final String CHEAT="ch";
    private static final String COOPERATE="co";


    public static MoveScore calculate(String inputOne , String inputTwo){
        MoveMapper moveMapper=MoveMapper.valueOf(inputOne+"_"+inputTwo);
        MoveScore moveScore = new MoveScore(moveMapper.getValue1() , moveMapper.getValue2());
        return moveScore;
    }
}
