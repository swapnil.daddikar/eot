package com.mapper;

public enum MoveMapper {

    ch_co(3,-1),
    co_ch(-1,3),
    co_co(2,2),
    ch_ch(0,0);


    private int value1;
    private int value2;

    MoveMapper(int value1,int value2){
       this.value1=value1;
       this.value2=value2;
    }


    public MoveMapper getValue(String move){
        return MoveMapper.valueOf(move);
    }

    public int getValue1(){
        return this.value1;
    }

    public int getValue2(){
        return this.value2;
    }







}
