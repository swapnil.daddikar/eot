package com.mapper;

public class MoveScore {


    private int playerOneMoveScore;
    private int playerTwoMoveScore;


    public MoveScore(int playerOneMoveScore , int playerTwoMoveScore){
        this.playerOneMoveScore=playerOneMoveScore;
        this.playerTwoMoveScore=playerTwoMoveScore;
    }


    public int getPlayerOneMoveScore() {
        return playerOneMoveScore;
    }

    public int getPlayerTwoMoveScore() {
        return playerTwoMoveScore;
    }


}
