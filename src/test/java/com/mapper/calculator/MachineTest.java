package com.mapper.calculator;

import com.machine.Machine;
import com.mapper.MoveScore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MachineTest {

    Machine machine = null;

    @Before
    public void initialize(){
        machine = new Machine();
    }

    @Test
    public void should_return_0_0_for_two_cheat_moves(){
        MoveScore moveScore = machine.calculate("ch" , "ch");
        Assert.assertEquals(0, moveScore.getPlayerOneMoveScore());
        Assert.assertEquals(0 , moveScore.getPlayerTwoMoveScore());
    }

    @Test
    public void should_return_2_2_for_two_cooperate_moves(){
        MoveScore moveScore = machine.calculate("co" , "co");
        Assert.assertEquals(2, moveScore.getPlayerOneMoveScore());
        Assert.assertEquals(2 , moveScore.getPlayerTwoMoveScore());
    }

    @Test
    public void should_return_3_negative_1_for_cheat_cooperate_move(){
        MoveScore moveScore = machine.calculate("ch" , "co");
        Assert.assertEquals(3, moveScore.getPlayerOneMoveScore());
        Assert.assertEquals(-1 , moveScore.getPlayerTwoMoveScore());
    }

    @Test
    public void should_return_negative_1_3_for_cooperate_cheat_move(){
        MoveScore moveScore = machine.calculate("co" , "ch");
        Assert.assertEquals(-1, moveScore.getPlayerOneMoveScore());
        Assert.assertEquals(3 , moveScore.getPlayerTwoMoveScore());
    }



}
