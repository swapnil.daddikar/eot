package com.player;

import com.game.GameScanner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Scanner;

public class ConsolePlayerTest {

    ConsolePlayer consolePlayer = null;

    @Before
    public void initialize(){
        consolePlayer = new ConsolePlayer("Player One" , 0, new GameScanner(new Scanner(System.in)));
    }

    @Test
    public void should_add_to_existing_score(){
        consolePlayer.updateScore(3);
        Assert.assertEquals(3, consolePlayer.getScore());
    }

    @Test
    public void should_subtract_from_existing_score(){
        consolePlayer.updateScore(-1);
        Assert.assertEquals(-1, consolePlayer.getScore());
    }




}
