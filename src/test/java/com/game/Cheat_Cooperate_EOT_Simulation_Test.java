package com.game;

import com.player.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class Cheat_Cooperate_EOT_Simulation_Test {


    private Player playerOne;
    private Player playerTwo;
    private GameScanner scanner;


    @Before
    public void initialize(){
        scanner= Mockito.mock(GameScanner.class);
        playerOne = new CheatPlayer("Player 1", 0 );
        playerTwo = new CooperatePlayer("Player 2", 0 );
    }

    @Test
    public void should_play_2_rounds_player_1_cheater_player_2_cooperate(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(6, playerOne.getScore());
        Assert.assertEquals(-2, playerTwo.getScore());
    }

    @Test
    public void should_play_5_rounds_player_1_cheater_player_2_cooperate(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(15, playerOne.getScore());
        Assert.assertEquals(-5, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_copycat_cheat_player_for_1_round(){
        playerTwo = new CopyCatPlayer("Player 2", 0 );
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(3, playerOne.getScore());
        Assert.assertEquals(-1, playerTwo.getScore());
    }


    @Test
    public void should_simulate_eot_game_for_copycat_cheat_player_for_2_rounds(){
        playerTwo = new CopyCatPlayer("Player 2", 0 );
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(3, playerOne.getScore());
        Assert.assertEquals(-1, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_copycat_cheat_player_for_5_rounds(){
        playerTwo = new CopyCatPlayer("Player 2", 0 );
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(3, playerOne.getScore());
        Assert.assertEquals(-1, playerTwo.getScore());
    }


}
