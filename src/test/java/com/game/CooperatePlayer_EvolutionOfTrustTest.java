package com.game;

import com.player.CheatPlayer;
import com.player.CooperatePlayer;
import com.player.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class CooperatePlayer_EvolutionOfTrustTest {


    private Player playerOne;
    private Player playerTwo;
    private GameScanner scanner;


    @Before
    public void initialize() {
        scanner = Mockito.mock(GameScanner.class);
        playerOne = new CooperatePlayer("Player 1", 0);
        playerTwo = new CooperatePlayer("Player 2", 0);
    }

    @Test
    public void should_simulate_eot_game_for_all_cheat_moves_3_round() {
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo, scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(6, playerOne.getScore());
        Assert.assertEquals(6, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_all_cooperate_moves_5_rounds() {
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo, scanner);
        Mockito.when(scanner.next()).thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("Y").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(10, playerOne.getScore());
        Assert.assertEquals(10, playerTwo.getScore());
    }


}
