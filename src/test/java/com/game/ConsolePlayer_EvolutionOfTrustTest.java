package com.game;

import com.player.ConsolePlayer;
import com.player.Player;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ConsolePlayer_EvolutionOfTrustTest {

    private Player playerOne;
    private Player playerTwo;
    private GameScanner scanner;


    @Before
    public void initialize(){
        scanner=Mockito.mock(GameScanner.class);
        playerOne = new ConsolePlayer("Player 1", 0 , scanner);
        playerTwo = new ConsolePlayer("Player 2", 0 , scanner);
    }

    @Test
    public void should_simulate_eot_game_for_all_cheat_moves_1_round(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("ch").thenReturn("ch").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(0, playerOne.getScore());
        Assert.assertEquals(0, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_all_cooperate_moves_2_rounds(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("co").thenReturn("co").thenReturn("Y").thenReturn("co").thenReturn("co").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(4, playerOne.getScore());
        Assert.assertEquals(4, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_cheat_cooperate_moves_2_rounds(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("ch").thenReturn("co").thenReturn("Y").thenReturn("ch").thenReturn("co").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(6, playerOne.getScore());
        Assert.assertEquals(-2, playerTwo.getScore());
    }

    @Test
    public void should_simulate_eot_game_for_cooperate_cheat_moves_for_2_rounds(){
        EvolutionOfTrust evolutionOfTrust = new EvolutionOfTrust(playerOne, playerTwo,scanner);
        Mockito.when(scanner.next()).thenReturn("co").thenReturn("ch").thenReturn("Y").thenReturn("co").thenReturn("ch").thenReturn("N");
        evolutionOfTrust.start();
        Assert.assertEquals(-2, playerOne.getScore());
        Assert.assertEquals(6, playerTwo.getScore());
    }

}
